#include <stdint.h>
#include <stdbool.h>

bool flag = false;

void timer_int() iv 0x0008 ics ICS_AUTO {
   TMR0H = 0xE1;
   TMR0L = 0x7B;    // 256 Hz - Preload = 57722.5 ~ 57723
   flag = true; // Set status flag
   INTCON.TMR0IF = 0;// Reset interrupt flag
}

void pwm_int() iv 0x0018 ics ICS_AUTO {
   TMR1H = 0xD8;
   TMR1L = 0xF0;    // 200 Hz - Preload = 55536
   LATB.B0 = ~LATB.B0; // Generate square wave 100 Hz (Calibration signal)
   PIR1.TMR1IF = 0;

}

void main() {

          TRISA = 0x01; // Set AN0 as input
          TRISB = 0x00;
          ADCON1 = 0x1E; // Set external reference, set AN0 as analog
          ADCON0 = 0x00; // Select AN0 channel
          ADCON2 = 0xBE; // Right justified result, acquisition time 20 Tad,
                         // Clock Fosc/64

          ADCON0.ADON = 1; // Enable ADC

          UART1_Init(115200);
          
          RCON.IPEN = 1; // Enable interrupt prirority

          INTCON = 0xE0;  // Enable high and low priority interrupts,
                          // Enable TMR0 overflow interrupt
                          
          PIE1.TMR1IE = 1; // Enable TMR1 overflow interrupt
          IPR1.TMR1IP = 0; // TMR1 low priority

          INTCON2 = 0x84; // TMR0 high priority
          
          T0CON = 0x08; // Timer0 Off, 16 bit, Internal clock
                        // Timer0 prescaller is not assigned
          
          TMR0H = 0xE1;
          TMR0L = 0x7B;    // 256 Hz - Preload = 57722.5 ~ 57723
          
          T1CON = 0x80;
          
          TMR1H = 0xD8;
          TMR1L = 0xF0;    // 200 Hz - Preload = 55536
          
          T0CON.TMR0ON = 1; // Enable TMR0
          T1CON.TMR1ON = 1; // Enable TMR1
          
          while(1){
                   while(!flag); // Wait for status flag
                   flag = false; // Reset status flag
                   ADCON0.GO_DONE = 1; // Start ADC conversion
                   while (!ADCON0.GO_DONE); // Wait for results from ADC module

                   UART1_Write(ADRESH);
                   UART1_Write(ADRESL); // Send data
                   
                   UART1_Write(0); // Add external button state (not implemented)*/

          }
}