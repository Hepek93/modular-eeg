#line 1 "C:/Users/Merenja/Desktop/EEG/MyProject.c"
#line 1 "c:/users/public/documents/mikroelektronika/mikroc pro for pic/include/stdint.h"




typedef signed char int8_t;
typedef signed int int16_t;
typedef signed long int int32_t;


typedef unsigned char uint8_t;
typedef unsigned int uint16_t;
typedef unsigned long int uint32_t;


typedef signed char int_least8_t;
typedef signed int int_least16_t;
typedef signed long int int_least32_t;


typedef unsigned char uint_least8_t;
typedef unsigned int uint_least16_t;
typedef unsigned long int uint_least32_t;



typedef signed char int_fast8_t;
typedef signed int int_fast16_t;
typedef signed long int int_fast32_t;


typedef unsigned char uint_fast8_t;
typedef unsigned int uint_fast16_t;
typedef unsigned long int uint_fast32_t;


typedef signed int intptr_t;
typedef unsigned int uintptr_t;


typedef signed long int intmax_t;
typedef unsigned long int uintmax_t;
#line 1 "c:/users/public/documents/mikroelektronika/mikroc pro for pic/include/stdbool.h"



 typedef char _Bool;
#line 4 "C:/Users/Merenja/Desktop/EEG/MyProject.c"
 _Bool  flag =  0 ;

void timer_int() iv 0x0008 ics ICS_AUTO {
 TMR0H = 0xE1;
 TMR0L = 0x7B;
 flag =  1 ;
 INTCON.TMR0IF = 0;
}

void pwm_int() iv 0x0018 ics ICS_AUTO {
 TMR1H = 0xD8;
 TMR1L = 0xF0;
 LATB.B0 = ~LATB.B0;
 PIR1.TMR1IF = 0;

}

void main() {

 TRISA = 0x01;
 TRISB = 0x00;
 ADCON1 = 0x1E;
 ADCON0 = 0x00;
 ADCON2 = 0xBE;


 ADCON0.ADON = 1;

 UART1_Init(115200);

 RCON.IPEN = 1;

 INTCON = 0xE0;


 PIE1.TMR1IE = 1;
 IPR1.TMR1IP = 0;

 INTCON2 = 0x84;

 T0CON = 0x08;


 TMR0H = 0xE1;
 TMR0L = 0x7B;

 T1CON = 0x80;

 TMR1H = 0xD8;
 TMR1L = 0xF0;

 T0CON.TMR0ON = 1;
 T1CON.TMR1ON = 1;

 while(1){
 while(!flag);
 flag =  0 ;
 ADCON0.GO_DONE = 1;
 while (!ADCON0.GO_DONE);

 UART1_Write(ADRESH);
 UART1_Write(ADRESL);

 UART1_Write(0);

 }
}
