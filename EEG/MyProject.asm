
_timer_int:

;MyProject.c,6 :: 		void timer_int() iv 0x0008 ics ICS_AUTO {
;MyProject.c,7 :: 		TMR0H = 0xE1;
	MOVLW       225
	MOVWF       TMR0H+0 
;MyProject.c,8 :: 		TMR0L = 0x7B;    // 256 Hz - Preload = 57722.5 ~ 57723
	MOVLW       123
	MOVWF       TMR0L+0 
;MyProject.c,9 :: 		flag = true; // Set status flag
	MOVLW       1
	MOVWF       _flag+0 
;MyProject.c,10 :: 		INTCON.TMR0IF = 0;// Reset interrupt flag
	BCF         INTCON+0, 2 
;MyProject.c,11 :: 		}
L_end_timer_int:
L__timer_int7:
	RETFIE      1
; end of _timer_int

_pwm_int:
	MOVWF       ___Low_saveWREG+0 
	MOVF        STATUS+0, 0 
	MOVWF       ___Low_saveSTATUS+0 
	MOVF        BSR+0, 0 
	MOVWF       ___Low_saveBSR+0 

;MyProject.c,13 :: 		void pwm_int() iv 0x0018 ics ICS_AUTO {
;MyProject.c,14 :: 		TMR1H = 0xD8;
	MOVLW       216
	MOVWF       TMR1H+0 
;MyProject.c,15 :: 		TMR1L = 0xF0;    // 200 Hz - Preload = 55536
	MOVLW       240
	MOVWF       TMR1L+0 
;MyProject.c,16 :: 		LATB.B0 = ~LATB.B0; // Generate square wave 100 Hz (Calibration signal)
	BTG         LATB+0, 0 
;MyProject.c,17 :: 		PIR1.TMR1IF = 0;
	BCF         PIR1+0, 0 
;MyProject.c,19 :: 		}
L_end_pwm_int:
L__pwm_int9:
	MOVF        ___Low_saveBSR+0, 0 
	MOVWF       BSR+0 
	MOVF        ___Low_saveSTATUS+0, 0 
	MOVWF       STATUS+0 
	SWAPF       ___Low_saveWREG+0, 1 
	SWAPF       ___Low_saveWREG+0, 0 
	RETFIE      0
; end of _pwm_int

_main:

;MyProject.c,21 :: 		void main() {
;MyProject.c,23 :: 		TRISA = 0x01; // Set AN0 as input
	MOVLW       1
	MOVWF       TRISA+0 
;MyProject.c,24 :: 		TRISB = 0x00;
	CLRF        TRISB+0 
;MyProject.c,25 :: 		ADCON1 = 0x1E; // Set external reference, set AN0 as analog
	MOVLW       30
	MOVWF       ADCON1+0 
;MyProject.c,26 :: 		ADCON0 = 0x00; // Select AN0 channel
	CLRF        ADCON0+0 
;MyProject.c,27 :: 		ADCON2 = 0xBE; // Right justified result, acquisition time 20 Tad,
	MOVLW       190
	MOVWF       ADCON2+0 
;MyProject.c,30 :: 		ADCON0.ADON = 1; // Enable ADC
	BSF         ADCON0+0, 0 
;MyProject.c,32 :: 		UART1_Init(115200);
	BSF         BAUDCON+0, 3, 0
	CLRF        SPBRGH+0 
	MOVLW       16
	MOVWF       SPBRG+0 
	BSF         TXSTA+0, 2, 0
	CALL        _UART1_Init+0, 0
;MyProject.c,34 :: 		RCON.IPEN = 1; // Enable interrupt prirority
	BSF         RCON+0, 7 
;MyProject.c,36 :: 		INTCON = 0xE0;  // Enable high and low priority interrupts,
	MOVLW       224
	MOVWF       INTCON+0 
;MyProject.c,39 :: 		PIE1.TMR1IE = 1; // Enable TMR1 overflow interrupt
	BSF         PIE1+0, 0 
;MyProject.c,40 :: 		IPR1.TMR1IP = 0; // TMR1 low priority
	BCF         IPR1+0, 0 
;MyProject.c,42 :: 		INTCON2 = 0x84; // TMR0 high priority
	MOVLW       132
	MOVWF       INTCON2+0 
;MyProject.c,44 :: 		T0CON = 0x08; // Timer0 Off, 16 bit, Internal clock
	MOVLW       8
	MOVWF       T0CON+0 
;MyProject.c,47 :: 		TMR0H = 0xE1;
	MOVLW       225
	MOVWF       TMR0H+0 
;MyProject.c,48 :: 		TMR0L = 0x7B;    // 256 Hz - Preload = 57722.5 ~ 57723
	MOVLW       123
	MOVWF       TMR0L+0 
;MyProject.c,50 :: 		T1CON = 0x80;
	MOVLW       128
	MOVWF       T1CON+0 
;MyProject.c,52 :: 		TMR1H = 0xD8;
	MOVLW       216
	MOVWF       TMR1H+0 
;MyProject.c,53 :: 		TMR1L = 0xF0;    // 200 Hz - Preload = 55536
	MOVLW       240
	MOVWF       TMR1L+0 
;MyProject.c,55 :: 		T0CON.TMR0ON = 1; // Enable TMR0
	BSF         T0CON+0, 7 
;MyProject.c,56 :: 		T1CON.TMR1ON = 1; // Enable TMR1
	BSF         T1CON+0, 0 
;MyProject.c,58 :: 		while(1){
L_main0:
;MyProject.c,59 :: 		while(!flag); // Wait for status flag
L_main2:
	MOVF        _flag+0, 1 
	BTFSS       STATUS+0, 2 
	GOTO        L_main3
	GOTO        L_main2
L_main3:
;MyProject.c,60 :: 		flag = false; // Reset status flag
	CLRF        _flag+0 
;MyProject.c,61 :: 		ADCON0.GO_DONE = 1; // Start ADC conversion
	BSF         ADCON0+0, 1 
;MyProject.c,62 :: 		while (!ADCON0.GO_DONE); // Wait for results from ADC module
L_main4:
	BTFSC       ADCON0+0, 1 
	GOTO        L_main5
	GOTO        L_main4
L_main5:
;MyProject.c,64 :: 		UART1_Write(ADRESH);
	MOVF        ADRESH+0, 0 
	MOVWF       FARG_UART1_Write_data_+0 
	CALL        _UART1_Write+0, 0
;MyProject.c,65 :: 		UART1_Write(ADRESL); // Send data
	MOVF        ADRESL+0, 0 
	MOVWF       FARG_UART1_Write_data_+0 
	CALL        _UART1_Write+0, 0
;MyProject.c,67 :: 		UART1_Write(0); // Add external button state (not implemented)*/
	CLRF        FARG_UART1_Write_data_+0 
	CALL        _UART1_Write+0, 0
;MyProject.c,69 :: 		}
	GOTO        L_main0
;MyProject.c,70 :: 		}
L_end_main:
	GOTO        $+0
; end of _main
